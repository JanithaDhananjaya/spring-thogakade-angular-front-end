import {Injectable} from '@angular/core';
import {Customer} from '../models/customer.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {OrderDTO} from '../models/orderDTO';

@Injectable()
export class OrderService {

  constructor(private http: HttpClient) {
  }

  getCustomerIds(): Observable<Array<Customer>> {
    return this.http.get<Array<Customer>>('http://localhost:8080/api/v1/customers');
  }

  saveOrder(order: OrderDTO): Observable<boolean> {
    return this.http.put<boolean>('http://localhost:8080/api/v1/orders', order);
  }
}
