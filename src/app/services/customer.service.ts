import {Injectable} from '@angular/core';
import {Customer} from '../models/customer.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';

@Injectable()
export class CustomerService {

  constructor(private http: HttpClient) {
  }

  getAllCustomers(): Observable<Array<Customer>> {
    return this.http.get<Array<Customer>>('http://localhost:8080/api/v1/customers');
  }

  saveCustomer(customer: Customer): Observable<boolean> {
    return this.http.put<boolean>('http://localhost:8080/api/v1/customers', customer);
  }

  deleteCustomer(id: string): Observable<boolean> {
    return this.http.delete<boolean>('http://localhost:8080/api/v1/customers/' + id);
  }

}
