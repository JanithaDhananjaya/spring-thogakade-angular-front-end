import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {Item} from '../models/item.model';


const URL = '/api/v1/items/';

@Injectable()

export class ItemService {

  constructor(private http: HttpClient) {
  }

  getAllItems(): Observable<Array<Item>> {
    return this.http.get<Array<Item>>('http://localhost:8080/api/v1/items');
  }

  saveItem(item: Item): Observable<boolean> {
    return this.http.put<boolean>('http://localhost:8080/api/v1/items', item);
  }

  deleteItem(id: string): Observable<boolean> {
    return this.http.delete<boolean>('http://localhost:8080/api/v1/items/' + id);
  }


  //
  // getTotalItems(): Observable<number> {
  //   return this.http.get<number>(MAIN_URL + URL + '/count');
  // }

}
