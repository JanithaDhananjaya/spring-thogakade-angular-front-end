import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';
import {Router} from '@angular/router';
import {User} from '../models/user';

const URL = '/api/v1/login';

@Injectable()
export class UserLoginService {

  constructor(private http: HttpClient, private router: Router) {
  }

  login(user: User): Observable<boolean> {
    return this.http.post<boolean>('http://localhost:8080/' + URL, user)
      .pipe(
        map((result) => {
          sessionStorage.setItem('token', result + '');
          if (result) {
            this.router.navigate(['/main']);
          }
          return result;
        })
      );
  }

  isAuthenticated(): boolean {
    if (sessionStorage.getItem('token')) {
      return sessionStorage.getItem('token') == 'false' ? false : true;
    }
  }

  logout(): void {
    sessionStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
