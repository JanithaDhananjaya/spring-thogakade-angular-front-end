import {Component, OnInit, ViewChild} from '@angular/core';
import {Customer} from '../../../models/customer.model';
import {CustomerService} from '../../../services/customer.service';
import {NgForm} from '@angular/forms';

@Component({
  templateUrl: './manage-customer.component.html',
  styleUrls: ['./manage-customer.component.css']
})
export class ManageCustomerComponent implements OnInit {

  customers: Array<Customer> = [];
  // manuallySelected: boolean = false;
  @ViewChild('frmCustomers') frmCustomers: NgForm;

  constructor(private customerService: CustomerService) {
  }

  ngOnInit() {
    this.loadAllCustomers();
  }

  loadAllCustomers(): void {
    this.customerService.getAllCustomers().subscribe(
      (result) => {
        this.customers = result;
        console.log(this.customers);
      }
    );
  }

  deleteCustomer(customer: Customer): void {
    if (confirm('Are you sure you want to delete this customer?')) {
      this.customerService.deleteCustomer(customer.id).subscribe(
        (result) => {
          if (result) {
            alert('Customer has been deleted successfully');
            this.loadAllCustomers();
          } else {
            alert('Failed to delete the customer');
          }
          this.loadAllCustomers();
        }
      );
    }
  }

}
