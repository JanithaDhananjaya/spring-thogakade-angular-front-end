import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {CustomerService} from '../../../services/customer.service';
import {Router} from '@angular/router';
import {Customer} from '../../../models/customer.model';
import {Item} from '../../../models/item.model';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent implements OnInit {
  selectedCustomer: Customer = new Customer();
  tempCustomer: Customer = null;
  // manuallySelected: boolean = false;
  @ViewChild('frmCustomers') frmCustomers: NgForm;

  constructor(private customerService: CustomerService) {
  }

  ngOnInit() {
  }

  saveCustomer(): void {
    this.customerService.saveCustomer(this.selectedCustomer).subscribe(
      (result) => {
        if (result) {
          alert('Customer has been saved successfully');
          this.clear();
        } else {
          alert('Failed to save the customer');
        }
      }
    );
  }

  clear(): void {
    this.selectedCustomer = new Customer();
  }

}
