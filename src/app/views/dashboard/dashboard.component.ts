import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../../services/customer.service';
import {ItemService} from '../../services/items.service';
import {OrderService} from '../../services/order.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  totalCustomers: number = 20;
  totalItems: number = 10;
  totalOrders: number = 10;

  constructor(private customerService: CustomerService, private itemService: ItemService, private orderService: OrderService) {
  }


  ngOnInit() {
  }

}
