import {Component, OnInit} from '@angular/core';
import {Customer} from '../../../models/customer.model';
import {CustomerService} from '../../../services/customer.service';
import {Order} from '../../../models/order.model';
import {OrderService} from '../../../services/order.service';
import {ItemService} from '../../../services/items.service';
import {Item} from '../../../models/item.model';
import {OrderDetail} from '../../../models/orderDetail';
import {ItemDTO} from '../../../models/itemDTO';
import {OrderDTO} from '../../../models/orderDTO';

@Component({
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.css']
})
export class PlaceOrderComponent implements OnInit {
  customers: Array<Customer> = [];
  selectedCustomer: Customer = new Customer();
  items: Array<Item> = [];
  selectedItem: Item = new Item();
  orderDetails: Array<OrderDetail> = [];
  orderDetail: OrderDetail;
  today = Date.now();
  total: number = 0;
  reqqty: number = 0;
  orderId: number = 0;

  constructor(private customerService: CustomerService, private itemService: ItemService, private orderService: OrderService) {
  }

  ngOnInit() {
    this.loadCustomerIds();
    this.loadItemIds();
  }

  loadCustomerIds(): void {
    this.customerService.getAllCustomers().subscribe(result => {
      this.customers = result;
    });
  }

  loadItemIds(): void {
    this.itemService.getAllItems().subscribe(result => {
      this.items = result;
    });
  }

  addItems(qty): void {
    this.total += qty * this.selectedItem.unitprice;
    const remainingQTY = this.selectedItem.qtyonhand - qty;
    this.reqqty = qty;
    this.orderDetail = new OrderDetail(qty, new ItemDTO(this.selectedItem.itemcode, this.selectedItem.description, remainingQTY, this.selectedItem.unitprice));
    this.orderDetails.push(this.orderDetail);
    document.getElementById('totalprice').setAttribute('value', this.total.toString());
    this.reqqty = 0;
  }

  removeItem(item) {
    const index = this.orderDetails.indexOf(item);
    this.orderDetails.splice(index, 1);
  }

  placeOrder(orderDate, orderId) {
    const orderDTO: OrderDTO = new OrderDTO(orderId, orderDate, this.selectedCustomer, this.orderDetails);
    this.orderService.saveOrder(orderDTO).subscribe(result => {
      if (result) {
        alert('Order has been saved successfully');
        this.clear();
      } else {
        alert('Failed to save the order');
      }
    });
  }

  clear(): void {
    this.selectedCustomer = new Customer();
    this.selectedItem = new Item();
    this.orderDetails = null;
    this.orderDetail = null;
    this.today = 0;
    document.getElementById('reqqty').setAttribute('value', '0');
    document.getElementById('orderid').setAttribute('value', this.orderId++ + '');
  }

}
