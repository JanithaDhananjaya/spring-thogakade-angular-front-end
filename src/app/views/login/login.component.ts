import {Component, OnInit} from '@angular/core';
import {UserLoginService} from '../../services/userLogin.service';
import {User} from '../../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = new User();
  failed: boolean;

  constructor(private userService: UserLoginService) {
  }

  ngOnInit() {
  }

  login(): void {
    this.userService.login(this.user).subscribe(
      (result) => {
        this.failed = !result;
      }
    );
  }

}
