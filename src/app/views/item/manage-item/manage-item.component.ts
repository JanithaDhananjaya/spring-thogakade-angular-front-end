import {Component, OnInit} from '@angular/core';
import {Item} from '../../../models/item.model';
import {ItemService} from '../../../services/items.service';

@Component({
  selector: 'app-manage-item',
  templateUrl: './manage-item.component.html',
  styleUrls: ['./manage-item.component.css']
})
export class ManageItemComponent implements OnInit {
  items: Array<Item> = [];

  constructor(private itemService: ItemService) {
  }

  ngOnInit() {
    this.loadAllItems();
  }

  loadAllItems(): void {
    this.itemService.getAllItems().subscribe(
      (result) => {
        this.items = result;
        console.log(this.items);
      }
    );
  }

  deleteItem(item: Item): void {
    if (confirm('Are you sure you want to delete this item?')) {
      this.itemService.deleteItem(item.itemcode).subscribe(
        (result) => {
          if (result) {
            alert('Item has been deleted successfully');
            this.loadAllItems();
          } else {
            alert('Failed to delete the item');
          }
          this.loadAllItems();
        }
      );
    }
  }

}
