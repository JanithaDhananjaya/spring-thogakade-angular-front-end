import {Component, OnInit, ViewChild} from '@angular/core';
import {Item} from '../../../models/item.model';
import {NgForm} from '@angular/forms';
import {ItemService} from '../../../services/items.service';
import {Customer} from '../../../models/customer.model';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.css']
})
export class CreateItemComponent implements OnInit {
  selectedItem: Item = new Item();
  @ViewChild('frmItems') frmItems: NgForm;

  constructor(private itemService: ItemService) {
  }

  ngOnInit() {
  }

  saveItem(): void {
    this.itemService.saveItem(this.selectedItem).subscribe(
      (result) => {
        if (result) {
          alert('Item has been saved successfully');
          this.clear();
        } else {
          alert('Failed to save the item');
        }
      }
    );
  }

  clear(): void {
    this.selectedItem = new Item();
  }

}
