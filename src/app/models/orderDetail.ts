import {ItemDTO} from './itemDTO';

export class OrderDetail {
  qty: number;
  itemDTO: ItemDTO;

  constructor(qty: number, itemDTO: ItemDTO) {
    this.qty = qty;
    this.itemDTO = itemDTO;
  }
}
