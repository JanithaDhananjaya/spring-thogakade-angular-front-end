export class Item {
  itemcode: string;
  description: string;
  qtyonhand: number;
  unitprice: number;
}
