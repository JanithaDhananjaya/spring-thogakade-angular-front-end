import {Customer} from './customer.model';
import {OrderDetail} from './orderDetail';

export class OrderDTO {
  oid: String;
  orderDate: String;
  orderDetailDTOs: Array<OrderDetail> = [];
  customerDTO: Customer;

  constructor(oid: String, orderDate: String, customerDTO: Customer, orderDetailDTOs: Array<OrderDetail>) {
    this.oid = oid;
    this.orderDate = orderDate;
    this.customerDTO = customerDTO;
    this.orderDetailDTOs = orderDetailDTOs;
  }
}
