export class Customer {
  id: string;
  name: string;
  address: string;
  imagePath: string;
}
