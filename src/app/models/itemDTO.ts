export class ItemDTO {
  itemcode: string;
  description: string;
  qtyonhand: number;
  unitprice: number;

  constructor(itemcode: string, description: string, qtyonhand: number, unitprice: number) {
    this.itemcode = itemcode;
    this.description = description;
    this.qtyonhand = qtyonhand;
    this.unitprice = unitprice;
  }
}
