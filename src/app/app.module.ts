import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {CustomerService} from './services/customer.service';
import {OrderService} from './services/order.service';
import {UserLoginService} from './services/userLogin.service';
import {ItemService} from './services/items.service';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {ManageCustomerComponent} from './views/customer/manage-customer/manage-customer.component';
import {CreateCustomerComponent} from './views/customer/create-customer/create-customer.component';
import {ManageItemComponent} from './views/item/manage-item/manage-item.component';
import {CreateItemComponent} from './views/item/create-item/create-item.component';
import {PlaceOrderComponent} from './views/order/place-order/place-order.component';
import {DashboardComponent} from './views/dashboard/dashboard.component';
import { LoginComponent } from './views/login/login.component';

const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'viewcustomer', component: ManageCustomerComponent},
  {path: 'createcustomer', component: CreateCustomerComponent},
  {path: 'viewitem', component: ManageItemComponent},
  {path: 'createitem', component: CreateItemComponent},
  {path: 'placaorder', component: PlaceOrderComponent},
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    ManageCustomerComponent,
    CreateCustomerComponent,
    ManageItemComponent,
    CreateItemComponent,
    PlaceOrderComponent,
    DashboardComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [CustomerService, OrderService, ItemService, UserLoginService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
